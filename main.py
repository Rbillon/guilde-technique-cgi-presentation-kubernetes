from flask import Flask
import os

app = Flask(__name__)


@app.route("/hello")
def hello():
    return os.environ.get("myEnvironmentVariable")


@app.route("/secret")
def secret():
    return os.environ.get("SECRET_USERNAME")


@app.route("/config")
def config():
    return os.environ.get("CONFIG_MAP")

if __name__ == "__main__":
    port = int(os.environ.get("PORT", 5000))
    app.run(debug=False, host='0.0.0.0', port=port)
